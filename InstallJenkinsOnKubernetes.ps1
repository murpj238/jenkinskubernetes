param (
    [string] $namespace,
    [string] $deploymentFile,
    [string] $serviceFile,
    [string] $groupName,
    [string] $registryName,
    [string] $clusterName
)
az aks get-credentials --resource-group $groupName --name $clusterName
$creds = Get-AzContainerRegistry -ResourceGroupName $groupName -Name $registryName
$username = Read-Host -Prompt "Please enter a Jenkins Admin Username"
$pass = Read-Host -Prompt "Please enter a Jenkins Admin Password"
kubectl.exe create secret docker-registry username --from-literal=USERNAME=$username --namespace=$namespace --docker-server="$($creds.Username).azurecr.io" --docker-username=$creds.Username --docker-password=$creds.Password --docker-email=jack.murphy@bearingpoint.com
kubectl.exe create secret docker-registry password --from-literal=PASSWORD=$pass --namespace=$namespace --docker-server="$($creds.Username).azurecr.io" --docker-username=$creds.Username --docker-password=$creds.Password --docker-email=jack.murphy@bearingpoint.com
kubectl.exe create -f $deploymentFile --namespace=$namespace
kubectl.exe create -f $serviceFile --namespace=$namespace