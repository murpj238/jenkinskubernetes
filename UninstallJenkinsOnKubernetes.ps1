[CmdletBinding()]
param (
    [string] $namespace,
    [string] $deploymentName,
    [string] $serviceName
)
kubectl.exe delete secret username --namespace=$namespace
kubectl.exe delete secret password --namespace=$namespace
kubectl.exe delete svc $serviceName --namespace=$namespace
kubectl.exe delete deploy $deploymentName --namespace=$namespace