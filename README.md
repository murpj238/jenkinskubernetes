# JenkinsKubernetes

Jenkins server hosted on a Kubernetes cluster.

To install and run this project use the PowerShell scripts. provided

REQUIREMENTS
-------------------
This requires Docker, Kubernetes and the Azure CLI

## Future plans

Replace the Powershell scripts with Terraform to set up the Kubernetes cluster hosting Jenkins on Azure.

<aside class="notice">
This Project is currently unfinished
</aside>